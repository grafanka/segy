import numpy as np
import csv


from files_manager import FileManager
from bad_shots import BadShots


def main():

    fm = FileManager('C:/Users/Anna/Desktop/SEG D/segy_examples/', '0-3n')
    bs = BadShots(0, 25)

    #streams = fm.read_segys_spec(list(set(bs.for_noise).union(set(bs.for_shift))))
    #streams = fm.read_segys_spec(bs.for_shift)
    streams = fm.read_segys_spec(bs.for_noise)
    #streams = fm.read_segys()

    # log = {}
    # log.update(bs.add_noise(streams))
    # log.update(bs.add_shift(streams))
    #bs.add_noise(streams)

    bs.add_noise_m(streams,3)
    #bs.add_shift_random(streams, 0, 2)
    #bs.add_shift(streams)


    fm.write_segys(streams)

    # suffix = 'shift_' + str(bs.shift_length) + '_noise_' + str(bs.noise_level_min)+ '-' + str(bs.noise_level_max)
    # FileManager.write_dict('log_' + suffix + '.csv', log)


if __name__ == '__main__':
        main()