import os
import csv

import numpy as np
from obspy import read


class FileManager():

    def __init__(self, directory, write_suf):

            self.files_names = os.listdir(path=directory)
            self.directory = directory
            self.full_names = [self.directory+x for x in self.files_names if os.path.isfile(self.directory+x)]
            self.write_suf = write_suf


    @staticmethod
    def write_list(filename, list):

        with open(filename, 'w', newline='') as csv_file:
            csv_writer = csv.writer(csv_file)

            for x in list:
                csv_writer.writerow([x])

    @staticmethod
    def write_dict(filename, dict):

        with open(filename, 'w', newline='') as csv_file:
            csv_writer = csv.writer(csv_file)

            for x in dict:
                csv_writer.writerow([x] + dict[x])

    @staticmethod
    def read_list(filename):

        with open(filename, newline='') as csvfile:
            csv_reader = csv.reader(csvfile, quotechar='|')

            return [x[0] for x in csv_reader]

    def read_segys(self):

        streams = []

        for x in self.full_names:
            st = read(x)

            st.name = x
            st.short_name = x[::-1][x[::-1].index('/') - 1::-1]

            st.datanp = FileManager.read_segy(st, 1)   # st - stream, 1 - type of sorting
            streams.append(st)
            print(x)

        return streams

    def read_segys_spec(self, list):

        streams = []


        for x in list:

            st = read(x)

            st.name = x
            st.short_name = x[::-1][x[::-1].index('/') - 1::-1]

            st.datanp = FileManager.read_segy(st, 1)   # st - stream, 1 - type of sorting
            streams.append(st)


        return streams

    @staticmethod
    def read_segy(stream, sorting):
        # dt = stream.traces[0].data.dtype
        # array = stream.data
        # num = [(x + 1) % 16 for x in range(5)]

        return np.array([x.data for x in stream.traces], dtype=stream.traces[0].data.dtype)

    def write_segys(self, streams):

        if os.path.exists(self.directory + 'bad_shots') == False:

            os.mkdir(self.directory + 'bad_shots')

        for st in streams:

            self.write_segy(st)


    def write_segy(self, stream):

        i = 0
        for x in stream:

            x.data = stream.datanp[i]
            i += 1


        s = stream.name
        filename = self.directory + 'bad_shots/' + self.write_suf + s[::-1][s[::-1].index('/') - 1::-1]
        stream.write(filename, format='segy')






