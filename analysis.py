import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from sklearn import svm
from sklearn import preprocessing


def train(X_train, nu='auto', gamma='auto'):
    """feature_1, feature_2 -numpy arrays  with features for seismogramm"""



    scaler = preprocessing.StandardScaler().fit(X_train)
    X_train_scaled = scaler.transform(X_train)

    clf = svm.OneClassSVM(nu=nu, kernel="rbf", gamma=gamma)
    clf.fit(X_train_scaled)

    return scaler, clf, X_train_scaled


def classify(scaler, clf, Y):

    Y_scaled = scaler.transform(Y)
    answer = clf.predict(Y_scaled)

    return clf, Y_scaled, answer


def visualize_color_predict(clf, data_scaled, answer):
    # answer - array with -1 (bad shot), 1 (good shot)
    xmin = -20
    xmax = 20


    xx, yy = np.meshgrid(np.linspace(xmin, xmax, 500), np.linspace(xmin, xmax, 500))
    # plot the line, the points, and the nearest vectors to the plane
    Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    plt.title("Novelty Detection, nu = " + str(clf.nu) +' gamma= ' + str(clf.gamma))
    plt.contourf(xx, yy, Z, levels=np.linspace(Z.min(), 0, 7), cmap=plt.cm.PuBu)
    a = plt.contour(xx, yy, Z, levels=[0], linewidths=2, colors='darkred')
    plt.contourf(xx, yy, Z, levels=[0, Z.max()], colors='palevioletred')

    s = 40

    good, bad = sort_by_answer_predict(data_scaled, answer)

    b1 = plt.scatter(good[:, 0], good[:, 1], c='white', s=s)
    b2 = plt.scatter(bad[:, 0], bad[:, 1], c='red', s=s)
    #c = plt.scatter(data_scaled[:, 0], data_scaled[:, 1], c='gold', s=s)

    plt.axis('tight')
    plt.xlim((xmin, xmax))
    plt.ylim((xmin, xmax))
    plt.xlabel('sum_in_window')
    plt.ylabel('despersion')
    plt.legend([a.collections[0], b1, b2],
               ["learned frontier", "good shots predicted",
               "bad shots predicted"],
              loc="upper left",
              prop=matplotlib.font_manager.FontProperties(size=11))
    # plt.xlabel(
    #    "error train: %d/200 ; errors novel regular: %d/40 ; "
    #    "errors novel abnormal: %d/40"
    #    % (n_error_train, n_error_test, n_error_outliers))
    plt.show()


def sort_by_answer_predict(data_scaled, answer):
    good =[]
    bad=[]
    i = 0
    for x in answer:
        if x == -1.0:
            bad.append(data_scaled[i])
        else:
            good.append(data_scaled[i])
        i +=1
    good= (np.array(good))
    bad = (np.array(bad))
    return good, bad


def visualize_color_real(clf, data_scaled, answer):
    # answer - array with -1 (bad shot), 1 (good shot)
    xmin = -20
    xmax = 20


    xx, yy = np.meshgrid(np.linspace(xmin, xmax, 500), np.linspace(xmin, xmax, 500))
    # plot the line, the points, and the nearest vectors to the plane
    Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    plt.title("Novelty Detection, nu = " + str(clf.nu) +' gamma= ' + str(clf.gamma))
    plt.contourf(xx, yy, Z, levels=np.linspace(Z.min(), 0, 7), cmap=plt.cm.PuBu)
    a = plt.contour(xx, yy, Z, levels=[0], linewidths=2, colors='darkred')
    plt.contourf(xx, yy, Z, levels=[0, Z.max()], colors='palevioletred')

    s = 40

    good, noise, shift = sort_by_answer_real(data_scaled, answer)

    g = plt.scatter(good[:, 0], good[:, 1], c='white', s=s)

    if noise != []:
        n = plt.scatter(noise[:, 0], noise[:, 1], c='blueviolet', s=s)
    else:
        n = []

    if shift != []:
        s = plt.scatter(shift[:, 0], shift[:, 1], c='gold', s=s)
    else:
        s=[]



    plt.axis('tight')
    plt.xlim((xmin, xmax))
    plt.ylim((xmin, xmax))
    plt.xlabel('sum_in_window')
    plt.ylabel('despersion')
    plt.legend([a.collections[0], g, n, s],
               ["learned frontier", "training observations",
               "shots with noise", "shots with shift"],
              loc="upper left",
              prop=matplotlib.font_manager.FontProperties(size=11))
    # plt.xlabel(
    #    "error train: %d/200 ; errors novel regular: %d/40 ; "
    #    "errors novel abnormal: %d/40"
    #    % (n_error_train, n_error_test, n_error_outliers))
    plt.show()


def sort_by_answer_real(data_scaled, answer):

    good = []
    noise =  []
    shift = []
    i = 0
    for x in answer:
        if x == 'g':
            good.append(data_scaled[i])
        if x == 'n':
            noise.append(data_scaled[i])
        if x == 's':
            shift.append(data_scaled[i])
        i +=1

    good = (np.array(good))
    noise = (np.array(noise))
    shift = (np.array(shift))

    return good, noise, shift