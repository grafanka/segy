import numpy as np
import matplotlib.pyplot as plt


def sum_in_window(streams, t0, t):

    for st in streams:

        st.sum_in_window = sum([sum(abs(x[t0:t+1:]))/max(abs(x[t0:t+1:])) if max(abs(x[t0:t+1:])) != 0 else 0 \
                                for x in st.datanp ])


def despersion(streams):

    for st in streams:

        st.despersion = sum(np.array([abs(np.var(x)) for x in st.datanp.T]))


def fft(streams):

    for st in streams:

        st_fft = np.array(np.abs([np.fft.rfft(x) for x in st.datanp.T]))
        # st_fft = st_fft / (np.amax(st_fft))
        result = []

        for u in st_fft:
            r = 0.
            for i in range(len(u) // 10, len(u) // 3):
                r +=u[i]
            result.append(abs(r))

        st.fft = sum(result)


def normalization_sum_abs(streams):

    for st in streams:
        data_t = st.datanp.T
        data_t = np.array([abs(x) for x in st.datanp])
        st.sum_norm_abs = sum([sum(x)//(max(x)) for x in data_t])


def normalization_sum(streams):

    for st in streams:
        data_t = st.datanp.T
        st.sum_norm = sum([sum(x) // (max(x)) for x in data_t])

# def normalization_sum_f(streams):
#
#     for st in streams:
#         st_sum = st.datanp[:len(st.datanp)//2].T
#         st.sum_norm = np.array([sum(x)//(max(x) + 1) for x in st_sum])


def vkf(streams, window = 5):

    for st in streams:
        data = st.datanp.T
        res = []
        for i in range(0, data.shape[0] - window):
            vals = data[i:i + window].T
            corr = []
            for j in range(0, vals.shape[0] - 1):
                corr.append(np.correlate(vals[j], vals[j+1]))
            res.append(abs(int(sum(corr)//(max(abs(np.array(corr))) + 1))))
        for i in range(0,window):
            res.append(abs(int(res[i-1])))


        st.vkf = sum(np.array(res))





def visualize(streams, type):

    for st in streams:
        feat = getattr(st, type)
        for x in feat:
            plt.plot(x)

        plt.show()
        input()


def visualize_2(streams, type):
    for st in streams:
        feat = getattr(st, type)
        plt.plot(feat)

        plt.show()
        input()