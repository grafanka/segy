import numpy as np
import analysis as an


def format_data(feature1__good, feature1__noise, feature1__shift,
                feature2__good, feature2__noise, feature2__shift):

    feature_1 = list(feature1__good) + list(feature1__noise)+list(feature1__shift)
    feature_2 = list(feature2__good) + list(feature2__noise) + list(feature2__shift)
    features_2d_array = np.array([feature_1] + [feature_2]).T

    answer = ['g' for x in range(len(feature1__good))] + ['n' for x in range(len(feature1__noise))] + \
                  ['s' for x in range(len(feature1__shift))]

    return features_2d_array, answer


def main():

    feature1__good = np.load('sum_in_window_good.npy')
    feature2__good = np.load('despersion_good.npy')

    feature1__noise = np.load('sum_in_window_noise.npy')
    feature2__noise = np.load('despersion_noise.npy')

    feature1__shift = np.load('sum_in_window_shift.npy')
    feature2__shift = np.load('despersion_shift.npy')

    X_train, ans = format_data(feature1__good, [], [], feature2__good,
                                       [], [])

    # X_train, answer_real = format_data(feature1__good, feature1__noise, feature1__shift, feature2__good,
    #                                    feature2__noise, feature2__shift)

    scaler, clf, X_train_scaled = an.train(X_train, nu=0.04, gamma=0.005)

    Y, answer_real = format_data(feature1__good, feature1__noise, feature1__shift, feature2__good,
                                 feature2__noise, feature2__shift)
    Y, answer_real = format_data(feature1__good, [], [], feature2__good,
                                         [], [])

    clf, Y_scaled, answer_predict = an.classify(scaler, clf, Y)

    an.visualize_color_real(clf, Y_scaled, answer_real)
    # an.visualize_color_real(clf, Y_scaled, answer_real)
    # an.visualize_color_predict(clf, Y_scaled, answer_predict)



if __name__ == '__main__':
    main()
