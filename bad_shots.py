import numpy as np

from files_manager import FileManager


class BadShots():

    noise_level_min = 0 # in percents
    noise_level_max = 0

    shift_length = 0

    def __init__(self, noise_level_min=0, noise_level_max=5, shift_length=2):

        self.noise_level_min = noise_level_min
        self.noise_level_max = noise_level_max

        self.shift_length = shift_length

        self.for_noise = FileManager.read_list('for_noise.csv')
        self.for_shift = FileManager.read_list('for_shift.csv')

    def add_noise_m(self, streams, nmax):
        """ad random noise from -nmax to nmax % (from max value) """

        nmax *=0.01
        nmin = -0.01*nmax

        #log = {}

        for st in streams:

            if st.name in self.for_noise:
                print('hey')

                m = np.median(abs(st.datanp[0]))
                st.datanp = np.array([[x + m*((nmax - nmin) * np.random.random() + nmin) for x in y]
                                            for y in st.datanp], dtype=st.datanp.dtype)


                #log[st.short_name] = ['bad', 'noise']


        # return log
    def add_noise(self, streams, nmax):
        """ad random noise from -nmax to nmax % (from max value) """

        nmax *=0.01
        nmin = 0

        #log = {}

        for st in streams:

            if st.name in self.for_noise:
                print('hey')


                st.datanp = np.array([[x + x*((nmax - nmin) * np.random.random() + nmin) for x in y]
                                            for y in st.datanp], dtype=st.datanp.dtype)


                #log[st.short_name] = ['bad', 'noise']


        # return log

    def add_shift(self, streams):

        l = len(streams[0].datanp[0])
        log = {}

        for st in streams:

            if st.name in self.for_shift:

                st.datanp = np.array([[5 for x in range(self.shift_length)]+list(x[0:(l-self.shift_length):])
                                 for x in st.datanp], dtype=st.datanp.dtype)

                log[st.short_name] = ['bad', 'shift']

        return log

    def add_shift_random(self, streams, smin, smax):

        l = len(streams[0].datanp[0])
        smin = (l * smin * 0.01)
        smax = (l * smax * 0.01)
        log = {}
        nmin = -10*0.01             # noise level in shifted area
        nmax = 10*0.01

        for st in streams:

            if st.name in self.for_shift:

                m = max(st.datanp[0])
                shift = round((smax - smin) * np.random.random() + smin)
                st.datanp = np.array([[m*((nmax - nmin) * np.random.random() + nmin) \
                                        for x in range(shift)]+list(x[0:(l-shift):])
                                        for x in st.datanp], dtype=st.datanp.dtype)


                #log[st.short_name] = ['bad', 'shift']

        #return log
