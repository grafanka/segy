import numpy as np

from files_manager import FileManager
from bad_shots import BadShots
import feature
import analysis
import matplotlib.pyplot as plt


def main():
    #
    fm = FileManager('C:/Users/Anna/Desktop/SEG D/segy_examples/bad_shots/noise/','')
    streams = fm.read_segys()

    feature.sum_in_window(streams, 0, 300)
    sum_in_window = np.array([x.sum_in_window for x in streams])
    np.save('sum_in_window_noise.npy', sum_in_window)


    feature.despersion(streams)
    despersion = np.array([x.despersion for x in streams])
    np.save('despersion_noise.npy', despersion)
    # #
    #
    # fm = FileManager('C:/Users/Anna/Desktop/SEG D/segy_examples/bad_shots/s/', '')
    # streams = fm.read_segys()
    #
    # feature.sum_in_window(streams, 0, 300)
    # sum_in_window = np.array([x.sum_in_window for x in streams])
    # np.save('sum_in_window_shift.npy', sum_in_window)
    #
    # #
    # feature.despersion(streams)
    # despersion = np.array([x.despersion for x in streams])
    # np.save('despersion_shift.npy', despersion)


    # fm = FileManager('C:/Users/Anna/Desktop/SEG D/segy_examples/bad_shots/good/', '')
    # streams = fm.read_segys()
    #
    # feature.sum_in_window(streams, 0, 2000)
    # sum_in_window = np.array([x.sum_in_window for x in streams])
    # np.save('sum_in_window_good.npy', sum_in_window)


    # feature.despersion(streams)
    # despersion = np.array([x.despersion for x in streams])
    # np.save('despersion_good.npy', despersion)



if __name__ == '__main__':
    main()