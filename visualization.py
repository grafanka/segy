import pylab
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

from files_manager import FileManager
from bad_shots import BadShots

def makeData (st):

    x0 = 70
    y0 = 1000
    x = np.array(range(x0))
    y = np.array(range(y0))
    xgrid, ygrid = np.meshgrid(x, y)

    zgrid = []

    z = st.datanp[0:x0]

    for x in z:
        zgrid.append((x[0:y0]))

    zgrid = np.array(zgrid)

    return xgrid.T, ygrid.T, zgrid


fm = FileManager('C:/Users/Anna/Desktop/SEG D/segy_examples/')
# bs = BadShots(0, 1, 500)

streams = fm.read_segys()
st = streams[0]
# bs.add_shift(st)
x, y, z = makeData(st)
plt.figure()
plt.xlabel('Traces')
plt.ylabel('t / dt')
plt.title(st.name[::-1][st.name[::-1].index('/') - 1::-1])
plt.contourf(x, y, z, cmap=pylab.get_cmap('gray_r'))
plt.show()

